import React from "react";
import { Link } from "react-router-dom";

class AddContact extends React.Component {
    state = {
        name:"",
        surname:"",
        birthday:""
    };
    add = (e) => {
        if (this.state.name === "" || this.state.surname === "" || this.state.birthday === "") {
            alert("Не все данные заполнены");
            return
        }
         this.props.addContactHandler(this.state);
         this.setState({name: "", surname: "",birthday: "" });
         this.props.history.push("/");
    }
    render() {
        return ( 
            <div className="ui main">
                <h2>Добавить контакт</h2>
                <form className="ui form" onSubmit={this.add}>
                    <div className="field">
                        <label>Имя</label>
                        <input
                         type="text"
                         name="name"
                         placeholder="Введите имя" 
                         value={this.state.name}
                         onChange={(e) => this.setState({name: e.target.value})}
                        />
                    </div>
                    <div className="field">
                        <label>Фамилия</label>
                        <input 
                         type="text" 
                         name="surname" 
                         placeholder="Введите фамилию"
                         value={this.state.surname}
                         onChange={(e)=> this.setState({surname:e.target.value})}
                        />
                    </div>
                    <div className="field">
                        <label>Год рождения</label>
                        <input 
                         type="text" 
                         name="birthday" 
                         placeholder="Введите год рождения"
                         value={this.state.birthday}
                         onChange={(e)=> this.setState({birthday: e.target.value})}
                        />
                    </div>
                    <button className="ui button blue">Добавить</button>
                    <Link to="/" class="ui animated button red" tabindex="0">
                    <div class="visible content">Назад</div>
                    <div class="hidden content">
                    <i class="right arrow icon"></i>
                    </div>
                    </Link>
                </form>
                
            </div>
        );
    }  
  };



export default AddContact;