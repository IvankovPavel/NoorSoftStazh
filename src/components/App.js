import React, {useState, useEffect} from "react";
import { BrowserRouter as Router,Switch,Route} from "react-router-dom";
import api from '../api/contacts';
import "./App.css";
import Header from "./Header";
import AddContact from "./AddContact";
import ContactList from "./ContactList";
import EditContact from "./EditContact";




function App(){
  const[contacts,setContacts] = useState([]);

  //RetrieveContacts
  const RetrieveContacts = async () => {
    const response = await api.get("/api/records");
    return response.data;
  };
  const updateContactHandler = async (contact) => {
    const response = await api.post(`/api/records/${contact.id}`, contact);
    const {id} = response.data;
    setContacts(
    contacts.map((contact)=>{
      return contact.id === id ? {...response.data} : contact;
    })
    );
  };

  const addContactHandler = async (contact) => {
    const request ={
      data: contact,
    }; 
    const response = await api.put("/api/records",request);
    setContacts([...contacts, response.data]);
  };

  const removeContactHandler = async (id) => {
    await api.delete(`/api/records/${id}`);
    const newContactList = contacts.filter((contact) => {
      return contact.id !== id;
    });
    setContacts(newContactList);
  };

  useEffect(() => {
    const getAllContacts = async() => {
      const allContacts = await RetrieveContacts();
      if(allContacts) setContacts(allContacts);
    };
    getAllContacts();
  },[]);

  useEffect(() => {
   // localStorage.setItem(LOCAL_STORAGE_KEY,JSON.stringify(contacts));
   },[contacts]);

  return(
    <div className="ui countainer">
      <Router>
       <Header/>
       <Switch>
        <Route
          path="/"
          exact
          render={(props) => (
            <ContactList 
              {...props} 
              contacts={contacts} 
              getContactId={removeContactHandler}
            />
          )} 
        />
        <Route 
          path="/add" 
          render={(props) => (
            <AddContact {...props} addContactHandler={addContactHandler}/>
            )}
            
        />
          <Route 
          path="/edit" 
          render={(props) => (
            <EditContact {...props} updateContactHandler={updateContactHandler}/>
            )}
        />
       </Switch>
      </Router>
    </div>
  )
}

export default App;
