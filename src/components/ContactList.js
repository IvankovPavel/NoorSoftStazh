import React from 'react';
import { Link } from "react-router-dom";
import ContactCard from './ContactCard';


const ContactList = (props) => {
    console.log(props);
    const deleteContactHandler = (id) =>{
        props.getContactId(id);
    };


    const renderContantList = props.contacts.map((contact) => {
        return (
            <ContactCard
             contact={contact.data} 
             clickHandler = {deleteContactHandler} 
             key={contact.id}
            />
        );
    });
    return (
        <div class="main">    
          <h2>Список контактов
              <Link to="/add">
                 <button className="ui button blue right" style={{ float:"right"}}>Добавить контакт</button>
              </Link>
          </h2>       
          <div className="ui celled list">{renderContantList}</div>
        </div> 
    );
};

export default ContactList;