import React from "react";
import { Link } from "react-router-dom";

class EditContact extends React.Component {
    constructor(props) {
        super(props)
        const{id,name,surname,birthday} =props.location.state.contact;
        this.state ={
            id,
            name,
            surname,
            birthday,
    
        };
    }

    update = (e) => {
        if (this.state.name === "" || this.state.surname === "" || this.state.birthday === "") {
            alert("Не все данные заполнены");
            return
        }
         this.props.updateContactHandler(this.state);
         this.setState({name: "", surname: "",birthday: "" });
         this.props.history.push("/");
    }
    render() {
        return ( 
            <div className="ui main">
                <h2>Обновить контакт</h2>
                <form className="ui form" onSubmit={this.update}>
                    <div className="field">
                        <label>Name</label>
                        <input
                         type="text"
                         name="name"
                         placeholder="Enter name" 
                         value={this.state.name}
                         onChange={(e) => this.setState({name: e.target.value})}
                        />
                    </div>
                    <div className="field">
                        <label>Surname</label>
                        <input 
                         type="text" 
                         name="surname" 
                         placeholder="Enter surname"
                         value={this.state.surname}
                         onChange={(e)=> this.setState({surname:e.target.value})}
                        />
                    </div>
                    <div className="field">
                        <label>Birthday</label>
                        <input 
                         type="text" 
                         name="birthday" 
                         placeholder="Enter birthday"
                         value={this.state.birthday}
                         onChange={(e)=> this.setState({birthday: e.target.value})}
                        />
                    </div>
                    <button className="ui button blue">Обновить</button>
                    <Link to="/" class="ui animated button red" tabindex="0">
                    <div class="visible content">Назад</div>
                    <div class="hidden content">
                    <i class="right arrow icon"></i>
                    </div>
                    </Link>
                </form>
                
            </div>
        );
    }  
  };



export default EditContact;